from django.urls import path
from . import views

app_name = "story3app"

urlpatterns = [
	path('', views.hello, name='home'),
	path('/project', views.another, name='project')
]