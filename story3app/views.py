from django.shortcuts import render

def hello(request):
	return render(request, 'hello.html')

def another(request):
	return render(request, 'another.html')

